
const classes = require("class");
const hidenseek = require("hidenseek");

let params = process.argv.map((item, i) => {
    if (i === 0 || i === 1) {
        let par = item.split("\\");
        return par[par.length - 1].split(".")[0];
    }
    else return item;
});

let cmd = params.slice(0,3).reduce((cmd, item, i) => {
        if(item) return cmd += ` ${item}`;
});

console.log(cmd);

switch (cmd) {
    case "node index" : {
        console.log("Формат команд: \n" +
            "node index [команда] \n" +
            "Команды: \n" +
            "hide [PATH] [JSON] Прячет случайное количесво \n" +
            "покемонов из списка в указанной папке \n" +
            "PATH \"./field\" Путь к папке в которую спрятать покемонов \n" +
            "JSON \"./pokemons.json\" Путь к списку покемонов в формате json \n" +
            "seek [PATH] Поиск покемонов в указанной папке и вывод \n" +
            "в виде списка PokemonList \n" +
            "PATH \"./field\" Путь к папке в которой искать покемонов"
        );
        break;
    }
    case "node index hide" : {
        const pokemons = require(params[4]);
        hidenseek.hide(params[3], pokemons);
        break;
    }
    case "node index seek" : {
        hidenseek.seek(params[3])
            .then((pokemonList) => {
                pokemonList.show();
            });
        break;
    }
    default : console.log("Ошибка! Неправильная команда!");

}











